FROM node:12.16
LABEL maintainer=this@parvezmrobin.com

RUN apt update

RUN yarn global add @vue/cli @nestjs/cli
