/**
 * Parvez M Robin
 * this@parvezmrobin.com
 * May 29, 2020
 */

import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { LoggerModule } from './logger/logger.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { ConstraintsModule } from './constraints/constraints.module';

const staticModule =
  process.env.NODE_ENV !== 'development'
    ? []
    : [
        // __dirname is server/dist
        ServeStaticModule.forRoot({
          rootPath: join(__dirname, '..', '..', 'client', 'dist'),
          serveRoot: '/',
        }),
      ];

@Module({
  imports: [
    ...staticModule,
    MongooseModule.forRoot(`mongodb://${process.env.DB_HOST || 'localhost'}/nest-vue-starter`),
    AuthModule,
    LoggerModule,
    ConstraintsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
