import { NestFactory } from '@nestjs/core';
import { BadRequestException, HttpStatus, ValidationError, ValidationPipe } from '@nestjs/common';
import { useContainer } from 'class-validator';
import * as helmet from 'helmet';
import * as rateLimit from 'express-rate-limit';
import { LoggerService } from './logger/logger.service';
import { startCase } from 'lodash';
import * as dotenv from 'dotenv';
dotenv.config();
import { AppModule } from './app.module';

const port = process.env.PORT || 3000;
const prefix = process.env.PREFIX;

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: false,
  });

  app.use(helmet());
  app.use(
    rateLimit({
      windowMs: 15 * 60 * 1000, // 15 minutes
      max: 300, // limit each IP to 300 requests per windowMs
    }),
  );

  app.setGlobalPrefix(prefix);
  if (process.env.NODE_ENV === 'development') {
    app.enableCors({
      origin: /:8080$/,
    });
  }

  app.useLogger(new LoggerService());

  app.useGlobalPipes(
    new ValidationPipe({
      forbidNonWhitelisted: true,
      exceptionFactory(errors: ValidationError[]) {
        const errorMessages: Record<string, string[]> = {};
        errors.forEach((error) => {
          const errors = Object.values(error.constraints).map((error) => {
            const words = error.split(' ');
            const field = startCase(words[0]);
            return `${field} ${words.slice(1).join(' ')}`;
          });
          errorMessages[error.property] = errors;
        });
        return new BadRequestException({
          statusCode: HttpStatus.BAD_REQUEST,
          message: errorMessages,
          error: 'Bad Request',
        });
      },
    }),
  );

  // This will cause class-validator to use the nestJS module resolution,
  // the fallback option is to spare ourselves from importing all the class-validator modules to nestJS
  useContainer(app.select(AppModule), {
    fallback: true,
    fallbackOnErrors: true,
  });

  await app.listen(port);
}

bootstrap().then(() => {
  console.log('Server started:', `http://localhost:${port}`);
});
