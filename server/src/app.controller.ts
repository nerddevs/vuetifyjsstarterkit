import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { Message } from './types';

@Controller('/v1')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/ping')
  getHello(): Message {
    return this.appService.getHello();
  }
}
