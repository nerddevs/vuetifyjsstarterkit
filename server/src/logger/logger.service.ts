/**
 * Parvez M Robin
 * this@parvezmrobin.com
 * May 29, 2020
 */

import { Injectable, Scope, Logger } from '@nestjs/common';
import { createLogger, format } from 'winston';
import * as DailyRotateFile from 'winston-daily-rotate-file';

const logger = createLogger({
  level: 'info',
  format: format.combine(
    format.timestamp(),
    format.printf(
      ({ level, message, context, trace, timestamp }) =>
        `${level} - ${timestamp} [${context}] ${message}${trace ? ' - ' : ''}${trace}`,
    ),
  ),
  transports: [
    //
    // - Write all logs with level `error` and below to `error.log`
    // - Write all logs with level `info` and below to `combined.log`
    //
    new DailyRotateFile({
      filename: 'error.log',
      datePattern: 'YYYY-MM-DD',
      dirname: './logs/%DATE%/',
      level: 'error',
    }),
    new DailyRotateFile({
      filename: 'combined.log',
      datePattern: 'YYYY-MM-DD',
      dirname: './logs/%DATE%/',
    }),
  ],
});

@Injectable({ scope: Scope.TRANSIENT })
export class LoggerService extends Logger {
  log(message: any, context?: string) {
    if (context !== 'RouterExplorer') {
      super.log(message, context);
    }

    logger.info({ message, context });
  }

  error(message: any, trace?: string, context?: string) {
    super.error(message, trace, context);

    logger.error({ message, trace, context });
  }

  warn(message: any, context?: string) {
    super.warn(message, context);

    logger.warn({ message, context });
  }

  debug(message: any, context?: string) {
    super.debug(message, context);

    logger.debug({ message, context });
  }

  verbose(message: any, context?: string) {
    super.verbose(message, context);

    logger.verbose({ message, context });
  }
}
