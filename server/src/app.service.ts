import { Injectable } from '@nestjs/common';
import { Message } from "./types";

@Injectable()
export class AppService {
  getHello(): Message {
    return {
      message: 'Hello World!',
    };
  }
}
