import {
  ValidationArguments,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { Injectable } from '@nestjs/common';
import { Connection } from 'mongoose';
import { InjectConnection } from '@nestjs/mongoose';

@ValidatorConstraint({ async: true, name: 'UniqueUsername' })
@Injectable()
export class UniqueFieldConstraint implements ValidatorConstraintInterface {
  constructor(@InjectConnection() private readonly connection: Connection) {}

  async validate(username: any, validationArguments: ValidationArguments) {
    const { constraints } = validationArguments;
    const [modelName, fieldName] = constraints;
    const model = this.connection.models[modelName];
    const existingEntity = await model.findOne({ [fieldName]: username });
    return !existingEntity;
  }

  defaultMessage(validationArguments?: ValidationArguments): string {
    return `${validationArguments.property} is already taken`;
  }
}
