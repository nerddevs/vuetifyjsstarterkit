import { Global, Module } from '@nestjs/common';
import { MatchFieldConstraint } from './match-field.constaint';
import { UniqueFieldConstraint } from './unique-field.constaint';

@Global()
@Module({
  imports: [MatchFieldConstraint, UniqueFieldConstraint],
  exports: [MatchFieldConstraint, UniqueFieldConstraint],
})
export class ConstraintsModule {}
