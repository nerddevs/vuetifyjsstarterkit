/**
 * Parvez M Robin
 * this@parvezmrobin.com
 * May 28, 2020
 */

import {
  ValidationArguments,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { Injectable } from '@nestjs/common';

@ValidatorConstraint({ name: 'MatchField' })
@Injectable()
export class MatchFieldConstraint implements ValidatorConstraintInterface {
  validate(value: any, args: ValidationArguments) {
    const [relatedPropertyName] = args.constraints;
    const relatedValue = (args.object as any)[relatedPropertyName];
    return value === relatedValue;
  }

  defaultMessage(validationArguments?: ValidationArguments): string {
    return `${validationArguments.property} should match ${validationArguments.constraints[0]}`;
  }
}
