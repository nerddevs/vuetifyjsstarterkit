/**
 * Parvez M Robin
 * this@parvezmrobin.com
 * May 28, 2020
 */

import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginDto } from './auth.dto';
import { CreateUserDto } from './users/user.dto';

@Controller('v1/auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('/login')
  async login(@Body() body: LoginDto) {
    const user = await this.authService.validateUser(body.username, body.password);
    return {
      user,
      token: await this.authService.genToken(user),
    };
  }

  @Post('/register')
  async createUser(@Body() createUserDto: CreateUserDto) {
    const createdUser = await this.authService.createUser(createUserDto);
    return {
      user: createdUser,
    };
  }
}
