/**
 * Parvez M Robin
 * this@parvezmrobin.com
 * May 28, 2020
 */
import { IsNotEmpty, IsString } from 'class-validator';

export class LoginDto {
  @IsNotEmpty()
  @IsString()
  username: string;
  @IsNotEmpty()
  @IsString()
  password: string;
}
