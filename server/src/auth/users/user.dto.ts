/**
 * Parvez M Robin
 * this@parvezmrobin.com
 * May 27, 2020
 */

import { IsNotEmpty, IsString, Validate } from 'class-validator';
import { MatchFieldConstraint } from '../../constraints/match-field.constaint';
import { UniqueFieldConstraint } from '../../constraints/unique-field.constaint';

export class CreateUserDto {
  @IsString()
  @IsNotEmpty()
  firstName: string;

  @IsString()
  @IsNotEmpty()
  lastName: string;

  @IsString()
  @IsNotEmpty()
  @Validate(UniqueFieldConstraint, ['User', 'username'])
  username: string;

  @IsString()
  @IsNotEmpty()
  password: string;

  @Validate(MatchFieldConstraint, ['password'])
  @IsNotEmpty()
  confirm: string;
}
