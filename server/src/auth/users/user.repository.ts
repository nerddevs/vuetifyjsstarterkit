/**
 * Parvez M Robin
 * this@parvezmrobin.com
 * May 27, 2020
 */

import { FilterQuery, Model } from 'mongoose';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { IUser, IUserDocument } from './user.interface';

@Injectable()
export class UserRepository {
  constructor(
    @InjectModel('User')
    private readonly userModel: Model<IUserDocument>,
  ) {}

  async createUser(userData: Omit<IUser, '_id'>): Promise<Omit<IUser, 'password'>> {
    const createdUserDocument = await new this.userModel(userData).save();
    const createdUser = createdUserDocument.toObject();
    delete createdUser.password;
    return createdUser;
  }

  async findUser({ username }, forcePassword = false) {
    const query = this.userModel.findOne({ username });
    if (!forcePassword) {
      query.select('-password');
    }
    const user = await query.lean().exec();
    if (!user) {
      throw new NotFoundException('User not found');
    }
    return user;
  }

  async findUsers(filter: FilterQuery<IUserDocument>) {
    return this.userModel
      .find(filter)
      .select('-password')
      .lean()
      .exec();
  }

  async userExists({ username }) {
    const userExists = await this.userModel
      .findOne({ username })
      .countDocuments()
      .exec();
    return !!userExists;
  }
}
