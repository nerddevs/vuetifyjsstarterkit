import { Controller, Get, UseGuards } from '@nestjs/common';
import { UserRepository } from './user.repository';
import { JwtAuthGuard } from '../jwt.guard';

@UseGuards(JwtAuthGuard)
@Controller('v1/users')
export class UsersController {
  constructor(private readonly userRepository: UserRepository) {}

  @Get('')
  async getUsers() {
    return {
      users: await this.userRepository.findUsers({}),
    };
  }

  @Get('/root')
  async getRoot() {
    return {
      root: await this.userRepository.findUser({ username: 'root' }),
    };
  }
}
