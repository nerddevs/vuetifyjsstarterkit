/**
 * Parvez M Robin
 * this@parvezmrobin.com
 * May 27, 2020
 */

import { Document, Types } from 'mongoose';

export interface IUser {
  _id: any;
  firstName: string;
  lastName: string;
  username: string;
  password: string;
  role: string | Types.ObjectId;
}

export interface IUserDocument extends IUser, Document {}
