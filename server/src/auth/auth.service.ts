/**
 * Parvez M Robin
 * this@parvezmrobin.com
 * May 28, 2020
 */

import { BadRequestException, Injectable, OnModuleInit } from '@nestjs/common';
import { RoleRepository } from './roles/role.repository';
import { UserRepository } from './users/user.repository';
import { IRole } from './roles/role.interface';
import { JwtService } from '@nestjs/jwt';
import { IUser } from './users/user.interface';
import { LoggerService } from '../logger/logger.service';
import { CreateUserDto } from './users/user.dto';
import { createHash } from 'crypto';

/**
 * AuthService ensures admin role and root user is created on server start.
 * This does nothing if they already exists.
 */
@Injectable()
export class AuthService implements OnModuleInit {
  constructor(
    private readonly roleRepository: RoleRepository,
    private readonly userRepository: UserRepository,
    private readonly jwtService: JwtService,
    private readonly loggerService: LoggerService,
  ) {
    this.loggerService.setContext('AuthService');
  }
  async onModuleInit(): Promise<any> {
    if (await this.userRepository.userExists({ username: 'root' })) {
      return;
    }

    let adminRole: IRole;
    if (await this.roleRepository.roleExists({ role: 'admin' })) {
      adminRole = await this.roleRepository.findRole({ role: 'admin' });
    } else {
      adminRole = await this.roleRepository.createRole({ role: 'admin' });
    }

    const passwordHash = AuthService._makeHash('root');

    await this.userRepository.createUser({
      firstName: 'root',
      lastName: 'root',
      username: 'root',
      password: passwordHash,
      role: adminRole._id,
    });
    this.loggerService.log(`Root user created`);
  }

  private static _makeHash(data: string) {
    return createHash('sha512')
      .update(data)
      .digest('hex');
  }

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.userRepository.findUser({ username }, true);
    const passwordHash = AuthService._makeHash(pass);
    if (user.password === passwordHash) {
      delete user.password;
      return user;
    }
    throw new BadRequestException("Username and password don't match");
  }

  async genToken(user: IUser) {
    const payload = { username: user.username, sub: user._id };
    return this.jwtService.sign(payload);
  }

  async createUser(user: CreateUserDto) {
    const role = await this.roleRepository.findRole({
      role: 'admin',
    });

    const passwordHash = AuthService._makeHash(user.password);
    const createdUser = await this.userRepository.createUser({
      ...user,
      password: passwordHash,
      role: role._id,
    });
    this.loggerService.log(`User ${createdUser.username} registered`);
    return createdUser;
  }
}
