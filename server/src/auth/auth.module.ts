/**
 * Parvez M Robin
 * this@parvezmrobin.com
 * May 27, 2020
 */

import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserRepository } from './users/user.repository';
import { UserSchema } from './users/user.schema';
import { UsersController } from './users/users.controller';
import { RolesController } from './roles/roles.controller';
import { RoleSchema } from './roles/role.schema';
import { RoleRepository } from './roles/role.repository';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';
import { jwtConstants } from './constants';
import { JwtModule } from '@nestjs/jwt';
import { AuthController } from './auth.controller';
import { JwtAuthGuard } from './jwt.guard';
import { JwtStrategy } from './jwt.strategy';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Role', schema: RoleSchema }]),
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '300d' },
    }),
  ],
  controllers: [UsersController, RolesController, AuthController],
  providers: [UserRepository, RoleRepository, AuthService, JwtStrategy, JwtAuthGuard],
  exports: [UserRepository],
})
export class AuthModule {}
