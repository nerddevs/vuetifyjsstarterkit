/**
 * Parvez M Robin
 * this@parvezmrobin.com
 * May 28, 2020
 */

import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {}
