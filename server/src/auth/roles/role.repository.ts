import { Model } from 'mongoose';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { IRole, IRoleDocument } from './role.interface';

type RoleFilter = { [K in keyof IRole]?: IRole[K] };

@Injectable()
export class RoleRepository {
  constructor(
    @InjectModel('Role')
    private readonly roleModel: Model<IRoleDocument>,
  ) {}

  async createRole(roleData: Omit<IRole, '_id'>): Promise<IRole> {
    const createdRole = await new this.roleModel(roleData).save();
    return createdRole.toObject();
  }

  async findRole(filter: RoleFilter): Promise<IRole> {
    const role = await this.roleModel
      .findOne(filter)
      .lean()
      .exec();
    if (!role) {
      throw new NotFoundException('role not found');
    }
    return role;
  }

  async roleExists(filter: RoleFilter) {
    const userExists = await this.roleModel
      .findOne(filter)
      .countDocuments()
      .exec();
    return !!userExists;
  }
}
