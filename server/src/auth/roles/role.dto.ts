/**
 * Parvez M Robin
 * this@parvezmrobin.com
 * May 27, 2020
 */

import { IRole } from './role.interface';
import { IsNotEmpty, IsString } from "class-validator";

export type CreateRoleDto = IRole;

export class FindRoleDto {
  @IsNotEmpty()
  @IsString()
  role: string;
}
