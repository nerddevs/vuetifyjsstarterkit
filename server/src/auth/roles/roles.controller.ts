import { Controller, Get, UseGuards } from '@nestjs/common';
import { RoleRepository } from './role.repository';
import { JwtAuthGuard } from '../jwt.guard';

@UseGuards(JwtAuthGuard)
@Controller('v1/roles')
export class RolesController {
  constructor(private readonly roleRepository: RoleRepository) {}

  @Get('/admin')
  async getAdmin() {
    return {
      role: await this.roleRepository.findRole({ role: 'admin' }),
    };
  }
}
