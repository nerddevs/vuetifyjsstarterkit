/**
 * Parvez M Robin
 * this@parvezmrobin.com
 * May 27, 2020
 */

import { Schema } from 'mongoose';

export const RoleSchema = new Schema(
  {
    role: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  },
);
