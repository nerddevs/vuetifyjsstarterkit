import { Test, TestingModule } from '@nestjs/testing';
import { RolesController } from './roles.controller';
import { RoleRepository } from './role.repository';
import { MongooseModule } from '@nestjs/mongoose';
import { RoleSchema } from './role.schema';

describe('Roles Controller', () => {
  let controller: RolesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(`mongodb://${process.env.DB_HOST}/test-db`),
        MongooseModule.forFeature([{ name: 'Role', schema: RoleSchema }]),
      ],
      controllers: [RolesController],
      providers: [RoleRepository],
    }).compile();

    controller = module.get<RolesController>(RolesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
