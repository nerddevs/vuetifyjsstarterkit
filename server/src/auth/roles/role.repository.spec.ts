import { RoleRepository } from './role.repository';
import { Test } from '@nestjs/testing';
import { MongooseModule } from '@nestjs/mongoose';
import { RoleSchema } from './role.schema';

describe('RoleRepository', () => {
  let roleRepository: RoleRepository;
  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(`mongodb://${process.env.DB_HOST}/test-db`),
        MongooseModule.forFeature([{ name: 'Role', schema: RoleSchema }]),
      ],
      providers: [RoleRepository],
    }).compile();

    roleRepository = moduleRef.get<RoleRepository>(RoleRepository);
  });

  it('should be defined', () => {
    expect(roleRepository).toBeDefined();
  });
});
