/**
 * Parvez M Robin
 * this@parvezmrobin.com
 * May 27, 2020
 */

import { Document } from 'mongoose';

export interface IRole {
  _id: any;
  role: string;
}

export interface IRoleDocument extends IRole, Document {}
