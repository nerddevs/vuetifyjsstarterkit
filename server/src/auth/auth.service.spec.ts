import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { UserRepository } from './users/user.repository';
import { RoleRepository } from './roles/role.repository';
import { MongooseModule } from '@nestjs/mongoose';
import { RoleSchema } from './roles/role.schema';
import { UserSchema } from './users/user.schema';

describe('AuthService', () => {
  jest.setTimeout(60000);

  let module: TestingModule;
  let service: AuthService;

  beforeEach(async () => {
    module = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(`mongodb://${process.env.DB_HOST}/test-db`),
        MongooseModule.forFeature([{ name: 'Role', schema: RoleSchema }]),
        MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
      ],
      providers: [AuthService, UserRepository, RoleRepository],
    }).compile();

    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', async () => {
    expect(service).toBeDefined();
  });
});
