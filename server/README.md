# Nest JS - Vue - Server

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

## Test

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```

## Features

This starter kit integrates
 - Basic module structure
 - MongoDB database connection
 - JWT authentication
 - Validation architecture
 - Logging
 - `dotenv` support
 - CORS support for development
 - Rate-limiting
