import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import { Menu, User } from '@/types';

Vue.use(Vuex);

type State = {
  menus: Menu[];
  user: null | User;
  token: string;
};

const menus: Menu[] = [
  {
    id: 'dashboard',
    name: 'Dashboard',
    url: '/dashboard',
  },
  {
    id: 'home',
    name: 'Home',
    children: [
      {
        id: 'home-child',
        name: 'Child Of Home',
        children: [
          {
            id: 'home-grand-child',
            name: 'Grand Child Of Home',
            url: '/home',
          },
        ],
      },
    ],
  },
];

export default new Vuex.Store<State>({
  strict: true,

  state: {
    menus,
    user: null,
    token: '',
  },

  mutations: {
    setAuthInfo(state, { user, token }) {
      state.user = user;
      state.token = token;
      window.localStorage.setItem('token', token);
      window.localStorage.setItem('user', JSON.stringify(user));
      axios.defaults.headers.common.Authorization = `Bearer ${token}`;
    },
  },

  actions: {
    login({ commit, dispatch }, { user, token }) {
      commit('setAuthInfo', { user, token });
    },
    localLogin({ commit }) {
      const token = window.localStorage.getItem('token');
      if (!token) {
        return;
      }
      const user = JSON.parse(window.localStorage.getItem('user') || 'null');
      commit('setAuthInfo', { user, token });
    },
    logout({ commit }) {
      commit('setAuthInfo', { user: null, token: '' });
    },
  },

  modules: {},
});
