export interface Menu {
  id: string;
  name: string;
  icon?: string;
  url?: string;
  isFavorite?: boolean;
  children?: Menu[];
}

export interface User {
  firstName: string;
  lastName: string;
  username: string;
  createdAt?: string;
  updatedAt?: string;
}
