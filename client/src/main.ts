/**
 * Parvez M Robin
 * this@parvezmrobin.com
 * May 29, 2020
 */

import Vue from 'vue';
import axios from 'axios';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;

if (process.env.NODE_ENV === 'production') {
  axios.defaults.baseURL = '/api/v1/';
} else {
  axios.defaults.baseURL = 'http://localhost:3000/api/v1/';
}

new Vue({
  store,
  router,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
