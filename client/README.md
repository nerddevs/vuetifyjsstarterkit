# Vuetify Starter Kit

## Project setup
If your local node version doesn't match the node version specified in `package.json`,
then you might want to use `docker`.
Do start a `docker` container, run the following.
```sh
./shell/deploy.sh # for unix
```
Or
```cmd
shell\deploy.bat # for windows
```
Now, run
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

To get full flavor of what it's doing, you might want to run **Bikribatta** app run 
in parallel at [localhost:3000](http://localhost:3000).

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
