import { shallowMount } from '@vue/test-utils';
// @ts-ignore
import App from '@/App.vue';

describe('App.vue', () => {
  it('renders successfully', () => {
    const wrapper = shallowMount(App, );
    expect(wrapper.text()).toBeTruthy();
  });
});
