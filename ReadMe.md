# Nest Vue Starter

It is a monorepo for quick-start general purpose web application

## Tech Stack

### Front End
- Vue JS
- Vue Router
- Vuex (Vue Store)
- Vuetify (Front End Component Library)
- PWA
- TypeScript
- Jest

### Back End
- Node JS
- Nest JS
- Passport (Authentication)
- Winston (Logger)
- TypeScript
- Jest
- Supertest

### Database
- Mongo DB @ 3.2

### Code Quality Tool
- ES Lint
- Prettier

### Platform
- Docker
- Docker Composer

## Installation
To start the docker container, for Unix, run
```sh
./shell/deploy.sh
```

For Windows, run
```cmd
shell\deploy.bat
```

This will build the container and start a bash shell.
Inside the shell, run
```sh
yarn
```
This will install the dependencies.

At this point, you might want to integrate eslint and prettier with your IDE / code editor.

Copy the `server/.env.example` file to `server/.env`.
Now, to start the server, run
```sh
yarn start
```

This will start the backend at [localhost:3000](http://localhost:3000)
and backend at [localhost:8080](http://localhost:8080).
Visit front end to get a preliminary overview.

## Testing

For frontend testing, run
```sh
yart test:client
```

For backend testing, run
```sh
yarn test:server
```

## Build & Deploy
Since, both front end and back end are in TypeScript, they both need compilation.
To build both front-end and back-end, run
```sh
yarn build
```

To deploy, run
```sh
yarn deploy
```
